# Mobile Go

This is a chrome extension, Mobile Go is usefull if you want to continue your web navigation on your smartphone.
How does it works?

- It toggles the widget when you click on the extention 🤖 
- Get the tab url
- Convert this url into a QR code using [QR code generator web API](http://goqr.me/api/)
- You just have to scan this QR code with your smartphone an open the link 🚀
- (Get domain palette using [Picular](https://picular.co/))

## Setup

1. Download the repository
2. Open Chrome and go to your [extensions](chrome://extensions/)
3. Click on the "Load unpacked extension..." button
4. Select the downloaded directory "qrCode-generator"